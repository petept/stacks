<?
class Time{
  function timeago($ptime) {
    $etime = time() - $ptime;
    
    if ($etime < 1) {
      return '0 seconds';
    }
    
    $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
                30 * 24 * 60 * 60       =>  'month',
                24 * 60 * 60            =>  'day',
                60 * 60                 =>  'hour',
                60                      =>  'minute',
                1                       =>  'second'
                );
    
    foreach ($a as $secs => $str) {
      $d = $etime / $secs;
      if ($d >= 1) {
	$r = round($d);
	return $r . ' ' . $str . ($r > 1 ? 's' : '');
      }
    }
  }

  function usrPostCount($id){
    $c=Post::model()->count('post_authorid =:id',array(':id' => $id)); /* replies and threads */
    $d=Post::model()->count('post_authorid =:id AND post_isthread=1',array(':id' => $id)); /* threads */
    $b=Post::model()->count('post_authorid =:id AND post_isthread=0',array(':id' => $id)); /* replies */
    if($c)
      return array($d, $c, $b); /* threads, replies and threads, replies */
    else
      return 0;
  }
  
}
?>