<?php

class ImageController extends Controller
{
  public function filters()
    {
        return array( 'accessControl' ); // perform access control for CRUD operations
    }

  public function accessRules()
    {
      return array(
		   /*
		    * Logged in users 
		    */
		   array('allow',
			 'actions'=>array('new'),
			 'users'=>array('@'),
			 ),
		   /*
		    * Everyone
		    */
		   array('allow',
			 'actions'=>array('index', 'view', 'thread'),
			 'users'=>array('*'),
			 ),
		   /*
		    * Deny everything not mentioned
		    */
		   array('deny',  // deny all users
			 'actions' => array('*'),
			 'users'=>array('*'),
			 ),
		   );
    }
  
  /*
   * Action to list posts
   * also search by criteria
   */
  public function actionIndex()
  {
    //$this->render('index');
    $tags=array();
    if(@$_GET['tags']){
      $q=$_GET['tags']; $loadAll=false; }
    else {
      $q=""; $loadAll=true; }
    $tags = explode(' ', preg_replace('!\s+!', ' ', str_replace(",", " ", trim($q))));
    $x=0;
    foreach($tags as $k=>$n){
      if(preg_match('/^(id|md5|idb|not|userid):(.+)$/', $n, $matches)){
	$selectors[$matches[1]] = $matches[2];
	unset($tags[$k]);
      }
    }
    /*
    echo "<h1>tags</h1>";
    print_r($tags);
    */
    $posts = Yii::app()->db->createCommand();
    $posts->selectDistinct('image_thumb_path, image_id, image_title, image_author, image_path, image_cached_tags, image_md5, width, height, image_size, thumb_width, thumb_height, image_thumb_size');
    $posts->from('images');
    $ref = array(
		 'id' => 'image_id',
		 'md5' => 'image_md5',
		 'userid' => 'image_author',
		 );
    $tarray = array();
    $tarray2 = array();
      foreach($tags as $k=>$tag){
	$posts->join('image_tags as t'.$k, 'images.image_id=t'.$k.'.id');
	if(strlen(strstr($tag, '*')) > 0){
	  $tag=strtr($tag, array('%'=>'\%', '_'=>'\_'));
	  $tarray[$k] = array('like', 't'.$k.'.tag', str_replace('*', '%', $tag));
	} 
	else {
	  $tarray2[':tag'.$k] = $tags[$k];
	  $tarray[$k] = 't'.$k.'.tag=:tag'.$k;	
	}
      }

      /*
	//DEBUG
	echo strstr('hell*o', '*');
      */
	array_unshift($tarray, 'and');
	/*
	echo "<h1>tarray</h1>";
	print_r($tarray);
	echo "<h1>tarray2</h1>";
	print_r($tarray2);
      */
    if(isset($selectors)){
      $leftoff = count($tarray);
      echo "<h2>tarray count</h2>";
        foreach($selectors as $s=>$h){
	if($s=="not"){
	  $posts->join('image_tags as t'.$leftoff, 'images.image_id=t'.$leftoff.'.id');
	  $tarray2[':tag'.$leftoff] = $h;
	  $tarray[$leftoff]  = 't'.$leftoff.'.tag != :tag'.$leftoff;
	}
	elseif($s=="idb"){
	  $a = explode(',', $h);
	  //echo $a[0];
	  if(@!$a[1]) $a[1] = "99999999";
	  $tarray[$leftoff] = "image_id BETWEEN ".$a[0]." AND ".$a[1]; 
	} 
	elseif($s=="id"){
	  if($h>0){$tarray[$leftoff] = "image_id = ".intval($h); }
	  elseif(preg_match('/^(=|>|<|>=|<=|!=)\d{0,4}$/', $h)){ $tarray[$leftoff] = $ref[$s].$h;}
	  else { $tarray[$leftoff] = "image_id > 0"; } 
	} 
	elseif($s=="userid"){
	  if($h>0) $tarray[$leftoff] = "image_author = ".intval($h); 
	  elseif(preg_match('/^(=|>|<|>=|<=|!=)\d{0,3}$/', $h)) $tarray[$leftoff] = $ref[$s].$h;
	  else  $tarray[$leftoff] = "image_author > 0"; 
	}
	else {
	  $tarray[$leftoff] = $ref[$s].$h; //[image_id].[=4]
	}
	$leftoff++;
      }
    }
    
    /*
      //DEBUG
      echo "<h1>tarray</h1>";
      print_r($tarray);
      echo $loadAll;
    */
    if($loadAll==false)
      $posts->where($tarray, $tarray2);
    else
      $posts->where('images.image_id > 0');
    $posts->order('images.image_id desc');
    $posts = $posts->queryAll();
    //$posts = (object) $posts;
    $tag_list = Yii::app()->db->createCommand();
    $tag_list->selectDistinct('tag as tag_name, (SELECT COUNT(*) FROM image_tags WHERE tag=tag_name) as tag_count');
    $tag_list->from('image_tags');
    $where_array = array('or');
    $i=1;
    foreach($posts as $n){
      $where_array[$i] = 'id='.$n['image_id'];
      $i++;
    }
    foreach($posts as $n){
      $tag_list->where($where_array);
    }
    $tag_list->order('tag_name desc');
    $tag_list = $tag_list->queryAll();
  
    
      //DEBUG
      echo "<h1>posts</h1>";
      print_r($posts);
      echo "<h1>tags</h1>";
      print_r($tag_list);
      $this->render('index', array('posts'=>$posts,'tag_list'=>$tag_list));
  }
  
  public function actionNew()
  {
    $x=0;
    $model=new Image;
    if(isset($_POST['Image']))
      {
	//$model->attributes=$_POST['Image'];
	$model->image=CUploadedFile::getInstance($model,'image');
	/* 
	 * Save image to file
	 */
	$model->image_title = $_POST['Image']['image_title'];
	$model->image_author = Yii::app()->user->id;
	$model->image_cached_tags = $_POST['Image']['image_cached_tags'];
	
	if($model->validate())
	  {
	    $s=md5($model->image->name);
	    $j=$model->image->extensionName;
	    
	    
	    /*
	     * Thumbnail image
	     */
	    
	    
	    $model->image_path = 'images/'.$s.'.'.$j;
	    $model->image->saveAs('images/'.$s.'.'.$j);
	    $model->image_md5 = md5_file('images/'.$s.'.'.$j);

	    

	    $thumb = new Imagick('images/'.$s.'.'.$j);
	    $thumb->scaleImage(140,140,true);
	    $thumb->writeImage('images/thumbs/t_'.$s.'.'.$j);
	    $thumb->destroy(); 
	    
	    list($model->width, $model->height) = getimagesize('images/'.$s.'.'.$j);

	    if($model->width>850){
	      $thumb = new Imagick('images/'.$s.'.'.$j);
	      $thumb->scaleImage(845,845,true);
	      $thumb->writeImage('images/scales/s_'.$s.'.'.$j);
	      $thumb->destroy(); 
	      $model->scaled = 'images/scales/s_'.$s.'.'.$j;
	    } else {
	      $model->scaled = 'images/'.$s.'.'.$j;
	    }

	    list($model->thumb_width, $model->thumb_height) = getimagesize('images/thumbs/t_'.$s.'.'.$j);
	    $model->image_size = filesize('images/'.$s.'.'.$j);
	    $model->image_thumb_size = filesize('images/thumbs/t_'.$s.'.'.$j);
	    
	    $model->image_thumb_path = 'images/thumbs/t_'.$s.'.'.$j;
	    
	    $model->save();


	    $a=Image::model()->findAll(
				       array(
					     'condition' => 'image_author=:id', 
					     'params' => array(':id'=>Yii::app()->user->id),
					     'order' => 'image_id desc',
					     'limit' => '1'
					     )
				       );
	    foreach($a as $r) $new_id = $r->image_id;
	    $tags = explode(" ", $_POST['Image']['image_cached_tags']);
	    
	    foreach($tags as $tag)
	      {
		$tags_model = new ImageTags;
		$tags_model->tag = $tag;
		$tags_model->id = $new_id;
		$tags_model->save();
	      }
	    $this->redirect(array('image/index'));
	  } else {
	  $x=1;
	  $this->render('create', array('model'=>$model));
	}

      }
    if($x==0)
      $this->render('create', array('model'=>$model));
  }
  public $post;
 
  public function actionView(){
    $post = Yii::app()->db->createCommand();
    $post->select('*');
    $post->from('images');
    $post->where('image_id=:id', array(':id'=>$_GET['id']));
    $post = $post->queryRow();
    $this->post = $post;
    $this->render('view', array('post'=>$post));
  }
  
}
