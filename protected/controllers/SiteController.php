<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionWoo(){
	  echo '<link rel="stylesheet/less" type="text/css" href="'.Yii::app()->request->baseUrl.'/css/main.less">
	<script type="text/javascript" src="'.Yii::app()->request->baseUrl.'/js/less-1.3.0.min.js"></script>';
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
	  $model = new Post;
	  $c = new CDbCriteria();
	  $c->condition = 'news_id > 0';
	  $c->order = 'news_id desc';
	  
	  $count=News::model()->count($c);
	  
	  $pages=new CPagination($count);
	  $pages->pageSize=5;
	  $pages->applyLimit($c);

	  $posts=News::model()->findAll($c);
    /* 
     * Posts per page
     */
	  $pages->pageSize=10;
	  $pages->applyLimit($c);
	  $posts=News::model()->findAll($c);
	  /*print_r(Yii::app()->user->mod_branches);*/
	  $this->render('index',array('model'=>$model,'posts'=>$posts, 'pages' => $pages));
		  }
	
	
	
	public function actionAjaxtags(){
	  $q = $_GET['q'];
	  
	  $posts = Yii::app()->db->createCommand();
	  $posts->select('tag');
	  $posts->from('image_tags');
	  $keyword=strtr($q, array('%'=>'\%', '_'=>'\_'));
	  $posts->where(array('like','tag','%'.$keyword.'%'));
	  $tags = $posts->queryAll();
	  //print_r($tags);
	   $tarray=array();
	  foreach($tags as $row){
	      $tarray[] = $row['tag'];
	  }
	  $tarray = array_unique($tarray);
	  
	  $s='';
	  $i=0;
	  foreach($tarray as $k=>$v){
	     $i++;
	    if($i !== count($tarray))
	      $s .= '"'.$v.'", ';
	    if($i == count($tarray))
	      $s .= '"'.$v.'"'; 
	   
	  }
	  header('Content-type: application/json');
	  echo '['.$s.']';
	}
	public function actionShow(){
	  print_r($_POST);
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate()){
			  $mod = new Iplog;
			  $mod->ip = $_SERVER['REMOTE_ADDR'];
			  $mod->user_id = Yii::app()->user->id;
			  $mod->save();
			  $this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		
		$this->render('login',array('model'=>$model));
	}

	public $id;
	public function actionProfile(){
	  $model = new Profile;
	  $id=Yii::app()->getRequest()->getQuery('id');
	  $this->id=$id;
	  $exists=Profile::model()->exists('profile_id =:id',array(':id' => $id));
	  $user_exists=User::model()->exists('id =:id',array(':id' => $id));
	  if($exists and $user_exists){
	    $models = Profile::model()->findAll(array(
						      'condition' => 'profile_id = :id',
						      'params' => array(':id' => $id),
						      ));
	    $user_models = User::model()->findAll(array(
						      'condition' => 'id = :id',
						      'params' => array(':id' => $id),
						      ));	    
	    $user = array();
	    
	    foreach($models as $m){
	      $user['aboutme'] = $m->aboutme;
	      $user['website'] = $m->website;
	      $user['privacy'] = $m->privacy;
	    }
	    foreach($user_models as $a){
	      $user['join'] = $a->join;
	      $user['birthday'] = $a->dob;
	      $user['username'] = $a->username;
	      $user['email'] = $a->email;
	      $user['id'] = $id;
	    }

	    /* $m2 array */

	   $m2= Yii::app()->db->createCommand()
	     ->select('*')
	     ->from('tbl_contacts')
	     ->join('tbl_profile', 'tbl_contacts.contact = tbl_profile.profile_id')
	     ->join('tbl_user', 'tbl_contacts.contact = tbl_user.id')
	     ->where('owner=:id', array(':id'=>$id))
	     ->queryAll();
	  
	   if(!Yii::app()->user->isGuest){
	    
	   $m3= Yii::app()->db->createCommand()
	     ->select('*')
	     ->from('tbl_contacts')
	     ->join('tbl_profile', 'tbl_contacts.contact = tbl_profile.profile_id')
	     ->join('tbl_user', 'tbl_contacts.contact = tbl_user.id')
	     ->where('owner=:id', array(':id'=>Yii::app()->user->id))
	     ->queryAll();
	   } else {
	     $m3 = array();
	   }
	   $logins = Iplog::model()->findAll(array(
						   'condition' => 'user_id = :id',
						   'params' => array(':id' => $id),
						   'order' => 'time desc',
						   'limit' => '1'
						   ));
	   foreach($logins as $row){
	     $last_login = strtotime($row->time);	     
	   }
	   if(@!$last_login) $last_login = 0;
	    $this->pageTitle=$user['username'].' / '.Yii::app()->name;
	    $this->render('view_profile', array('user' => $user, 'contacts' => $m2, 'viewer_contacts' => $m3, 'last_login' => $last_login));
	  }
	  elseif($user_exists and !$exists){
	    Yii::app()->user->setFlash('error', "This user has no profile.");
	    $this->render('usr_error');
	      }
	  elseif(!$user_exists){
	    Yii::app()->user->setFlash('error', "Sorry, this user doesn't exist.");
	    $this->render('usr_error');
	  }
	}

	public function actionAddc(){
	  $id = $_GET['c'];
	  if(!isset($_POST['confirm'])){
	      $this->render('confirmadd', array('id'=>$id));
	    } else {
	      if($_POST['confirm']['sure'] == 1){
		if(!Yii::app()->user->isGuest){
		  
		  if($id != Yii::app()->user->id){
		    $m3= Yii::app()->db->createCommand()
		      ->select('*')
		      ->from('tbl_profile')
		      ->where('profile_id=:id', array(':id'=>$id))
		      ->queryRow();
		    
		    $m4= Yii::app()->db->createCommand() //return true if the user is already a contact
		      ->select('*')
		      ->from('tbl_contacts')
		      ->where('owner=:id and contact=:id2', array(':id'=>Yii::app()->user->id, 'id2' => $id))
		      ->queryAll();
		    
		    if(count($m4) == 0){
		      if($m3['profile_id']>0){
			$model = new Contacts;
			$model->owner = Yii::app()->user->id;
			$model->contact = $id;
			$model->save();
			$this->redirect('/testdrive/site/profile/'.$id);
		      } else 
			throw new CHttpException(404,'The specified user could not be found.');
		    } else 
		      throw new CHttpException(404,'This user is already in your contacts list.');
		  } else
		    throw new CHttpException(404,'You can not add yourself to your contacts.');
		} else 
		  throw new CHttpException(404,'Plese log in to add contacts.');	
	      } else {
		$this->redirect('/testdrive');
	    }
	    }
	}

	public function actionDelc(){
	  $id=$_GET['id'];
	  if(!isset($_POST['confirm'])){
	      $this->render('confirmdel', array('id'=>$id));
	    } else {
	      if($_POST['confirm']['sure'] == 1){
	  $m4= Yii::app()->db->createCommand() //return true if the user is already a contact
	    ->select('*')
	    ->from('tbl_contacts')
	    ->where('owner=:id and contact=:id2', array(':id'=>Yii::app()->user->id, 'id2' => $id))
	    ->queryAll();
	  
	  if(count($m4) > 0){
	    if(!Yii::app()->user->isGuest){
	      Contacts::model()->deleteAll(array(
					     'condition' => 'contact = :id and owner = :id2',
					     'params' => array(':id' => $id, 'id2' => Yii::app()->user->id),
					     ));
	      $this->redirect('/testdrive/site/profile/'.Yii::app()->user->id);
	    } else {
	      throw new CHttpException(404,'Plese log in to remove contacts.');
	    }
	  } else {
	    throw new CHttpException(404,'The specified user could not be found.');
	  }
	      } else
		$this->redirect('/testdrive/profile/'.Yii::app()->user->id);
	  }
	}
	
	public function actionSettings(){
	  $model=new Profile;
	  if(!Yii::app()->user->isGuest){
	    $id = Yii::app()->user->id;
	    $exists=Profile::model()->exists('profile_id =:id',array(':id' => $id));
	    $models = Profile::model()->findAll(array(
						      'condition' => 'profile_id = :id',
						      'params' => array(':id' => $id),
						      ));
	    $logins = Iplog::model()->findAll(array(
						      'condition' => 'user_id = :id',
						      'params' => array(':id' => $id),
						      ));
	    $user=array();
	    if(!$exists){
	      $user['aboutme'] = '';
	      $user['website'] = '';
	      $user['privacy'] = 1;
	      $user['css'] = '';
	    } else {
	      foreach($models as $m){
		$user['aboutme'] = $m->aboutme;
		$user['website'] = $m->website;
		$user['privacy'] = $m->privacy;
		$user['css'] = $m->css;
	      }     
	    }
	    
	    if(isset($_POST['Settings'])){
	      $a=0;
	      $model->attributes = $_POST['Settings'];
	      $model->profile_id = $id;
	      if($model->validate()){
		if($exists){
		  $w=Profile::model()->findByPk($id);
		  $w->aboutme = $_POST['Settings']['aboutme'];
		  $w->website = $_POST['Settings']['website'];
		  $w->privacy = $_POST['Settings']['privacy'];
		  $w->css = $_POST['Settings']['css'];
		  $w->save();
		  $x=1;
		  Yii::app()->user->setFlash('success', "Profile updated.");
		  $this->redirect(array('profile', 'id' => $id));
		} else {
		  $model->save();
		  $x=1;
		  Yii::app()->user->setFlash('success', "Profile saved.");
		  $this->redirect(array('profile', 'id' => $id));
		}
	      } else {
		$a=1;
		$user['aboutme'] = $_POST['Settings']['aboutme'];
		$user['website'] = $_POST['Settings']['website'];
		$user['privacy'] = $_POST['Settings']['privacy'];
		$user['css'] = $_POST['Settings']['css'];
		$this->render('user_settings', array('user' => $user, 'model' => $model, 'logins' => $logins));
	      }
	      if($a==0){
		$models = Profile::model()->findAll(array(
							  'condition' => 'profile_id = :id',
							  'params' => array(':id' => $id),
							  ));	      
		
		foreach($models as $m){
		  $user['aboutme'] = $m->aboutme;
		  $user['website'] = $m->website;
		  $user['privacy'] = $m->privacy;
		  $user['css'] = $m->css;
		}
		$this->render('user_settings', array('user' => $user, 'model' => $model, 'logins' => $logins));
	
		/* form submitted */
	      }
	    } else {
		/* fresh page load */
		$this->render('user_settings', array('user' => $user, 'model' => $model, 'logins' => $logins));
		
	      }
	    }
	  }
	
	public function actionUsrstyle(){
	  $id=$_GET['id'];
	  $w=Profile::model()->findByPk($id);
	  $exists=Profile::model()->exists('profile_id =:id',array(':id' => $id));
	  if($exists)
	    $css=$w->css;
	  else
	    $css='';
	  header('Content-type: text/css');
	  echo $css;
	}

	public function actionUpjax(){
	  
	}
	  
	public function actionNewusr(){
	  $model = new Regist;
	  $done = 0;
	  if(isset($_POST['Registration'])){
	    $model->email = $_POST['Registration']['email'];
	    $model->password = $_POST['Registration']['password'];
	    $model->password_confirm = $_POST['Registration']['password_confirm'];
	    $model->username = $_POST['Registration']['username'];
	    $model->dob = $_POST['Registration']['dob'];
	    if($model->validate()){
	      $model->save();
	      $this->render('done_registration', array('email' => $model->email));
	      $done=1;
	    } else {
	      $this->render('register',array('model'=>$model));
	      $done=1;
	    }
	  }
	  if(!$done){
	    $this->render('register',array('model'=>$model));
	  }	
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
	  Yii::app()->user->logout();
	  $this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionTest(){
	    $tags=array();
	    $q=$_GET['tags'];
	    $tags = explode(' ', $q);
	    $x=0;
	    foreach($tags as $k=>$n){
	      if(preg_match('/^(id|md5|idb|not|userid):(.+)$/', $n, $matches)){
		$selectors[$matches[1]] = $matches[2];
		unset($tags[$k]);
	      }
	    }
	    echo "<h1>tags</h1>";
	    print_r($tags);
	    
	    $posts = Yii::app()->db->createCommand();
	    $posts->selectDistinct('*');
	    $posts->from('images');
	    $ref = array(
			 'id' => 'image_id',
			 'md5' => 'image_md5',
			 'userid' => 'image_author',
			 );
	    $tarray = array();
	    $tarray2 = array();
	    foreach($tags as $k=>$tag){
	      echo '<br />'.$k.'=>'.$tag;
	      $posts->join('image_tags as t'.$k, 'images.image_id=t'.$k.'.id');
	      if(strlen(strstr($tag, '*')) > 0){
		$tag=strtr($tag, array('%'=>'\%', '_'=>'\_'));
		$tarray[$k] = array('like', 't'.$k.'.tag', str_replace('*', '%', $tag));
	      } else {
		$tarray2[':tag'.$k] = $tags[$k];
		$tarray[$k] = 't'.$k.'.tag=:tag'.$k;	
	      }
	    }
	    
	    echo strstr('hell*o', '*');
	    array_unshift($tarray, 'and');
	    echo "<h1>tarray</h1>";
	    print_r($tarray);
	    echo "<h1>tarray2</h1>";
	    print_r($tarray2);
	    
	    if(isset($selectors)){
	      $leftoff = count($tarray);
	      echo "<h2>tarray count</h2>";
	      echo $leftoff;
	      foreach($selectors as $s=>$h){
		if($s=="not"){
		  $posts->join('image_tags as t'.$leftoff, 'images.image_id=t'.$leftoff.'.id');
		  $tarray2[':tag'.$leftoff] = $h;
		  $tarray[$leftoff]  = 't'.$leftoff.'.tag != :tag'.$leftoff;
		}
		elseif($s=="idb"){
		  $a = explode(',', $h);
		  //echo $a[0];
		  if(@!$a[1]) $a[1] = "99999999";
		  $tarray[$leftoff] = "image_id BETWEEN ".$a[0]." AND ".$a[1]; 
		} elseif($s=="id"){
		  if($h>0){$tarray[$leftoff] = "image_id = ".intval($h); }
		  elseif(preg_match('/^(=|>|<|>=|<=|!=)\d{0,4}$/', $h)){ $tarray[$leftoff] = $ref[$s].$h;}
		  else { $tarray[$leftoff] = "image_id > 0"; } 
		} elseif($s=="userid"){
		  if($h>0){$tarray[$leftoff] = "image_author = ".intval($h); }
		  elseif(preg_match('/^(=|>|<|>=|<=|!=)\d{0,3}$/', $h)){ $tarray[$leftoff] = $ref[$s].$h;}
		  else { $tarray[$leftoff] = "image_author > 0"; }
		}
		else {
		  $tarray[$leftoff] = $ref[$s].$h; //[image_id].[=4]
		}
		$leftoff++;
	      }
	    }
	   

	    echo "<h1>tarray</h1>";
	    print_r($tarray);
	    
	    $posts->where($tarray, $tarray2);
	    
	    $posts->order('images.image_id desc');
	    $posts = $posts->queryAll();
	    //$posts = (object) $posts;
	    $tag_list = Yii::app()->db->createCommand();
	    $tag_list->selectDistinct('tag as tag_name, (SELECT COUNT(*) FROM image_tags WHERE tag=tag_name) as tag_count');
	    $tag_list->from('image_tags');
	    $where_array = array('or');
	    $i=1;
	    foreach($posts as $n){
	      $where_array[$i] = 'id='.$n['image_id'];
	      $i++;
	    }
	    foreach($posts as $n){
	      $tag_list->where($where_array);
	    }
	    $tag_list->order('tag_name desc');
	    $tag_list = $tag_list->queryAll();
	    echo "<h1>posts</h1>";
	    print_r($posts);
	    echo "<h1>tags</h1>";
	    print_r($tag_list);
	  }
}
