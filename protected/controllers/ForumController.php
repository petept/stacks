<?
class ForumController extends Controller {

      public function filters()
    {
        return array( 'accessControl' ); // perform access control for CRUD operations
    }

  public function accessRules()
    {
        return array(
		     /*
		      * Logged in users 
		      */
		     array('allow',
			   'actions'=>array('new'),
			   'users'=>array('@'),
			   ),
		     /*
		      * Everyone
		      */
		     array('allow',
			   'actions'=>array('index', 'view', 'thread'),
			   'users'=>array('*'),
			   ),
		     /*
		      * Deny everything not mentioned
		      */
		     array('deny',  // deny all users
			   'actions' => array('*'),
			   'users'=>array('*'),
			   ),
		     );
    }
  
  public function actionIndex(){
    /*
    $image = Yii::app()->image->load('images/test.jpg');
    $image->resize(400, 100)->rotate(-45)->quality(75)->sharpen(20);
    $image->save('images/small.jpg'); // or $image->save('images/small.jpg');
    */
    $thumb = new Imagick('images/test.jpg');

    $thumb->scaleImage(140,140,true);
    $thumb->writeImage('images/c.jpg');
    
    $thumb->destroy(); 
    
    $model=new Forum;
    $forums=Forum::model()->findAll(
				    array(
					  'select'=>'forum_name, forum_description, forum_posts, forum_id',
					  'condition'=>'forum_parent=0',
					  )
				    ); 
    $this->render('index', array('forums' => $forums));
  }
  
  /*
   * View a topic 
   */
  public function actionView(){
    $id = $_GET['id'];
    $forum=Forum::model()->findAll(
				   array(
					 'condition' => 'forum_id=:id', 
					 'params' => array(':id'=>$id)
					 )
				   );
    
    if($forum){
    foreach($forum as $row){
      $name = $row->forum_name;
    }
    $this->breadcrumbs = array(
        'Board index'=>array('forum/index'),
	$name
			       );
      $c = new CDbCriteria();
      $c->condition = 'post_forumid=:id AND post_isthread=1';
      $c->params = array(':id' => $id);
      $c->order = 'post_time desc';

      $count=Post::model()->count($c);
      $pages=new CPagination($count);
      
      $pages->pageSize=25;
      $pages->applyLimit($c);
      $posts=Post::model()->with('user', 'profile')->findAll($c);
      
      $breadcrumbs=array(
			 
			 );
      $counts=array();
      $last_replies = array();
      foreach($posts as $post){
	$a = new CDbCriteria();
	$a->condition = 'post_parent=:id AND post_isthread=0';
	$a->params = array(':id'=>$post->post_id);
	$p=new Post;
	$count = $p->count($a);
	$counts[$post->post_id] = $count;

	$a = new CDbCriteria();
	$a->condition = 'post_parent=:id';
	$a->params = array(':id'=>$post->post_id);
	$a->order = 'post_time desc';
	$a->limit = '1';
	$t = Post::model()->with('user', 'profile')->findAll($a);
	
	foreach($t as $x){
	  $authorid = $x->post_authorid;
	  $user = $x->user->username;
	  $time = $x->post_time;
	}
	$last_replies[$post->post_id] = array($authorid, $user, $time);

      }
      //print_r($last_replies);
      $this->render('view_forum', array(
					'forum_id' => $id,
					'posts' => $posts,
					'pages' => $pages,
					'counts' => $counts,
					)
		    );
      
    } else {
      Yii::app()->user->setFlash('cantfind', "Forum id $id could not be found.");
      $this->redirect('/testdrive/forum');
    }
    
  }

  /*
   * Nigga, look at that private function
   * this displays the thread, can only be called from other functions
   * used with aThread() to render the views in different circumstances without repeting the same code.
   */
  private function dpage($id, $model, $title, $forum){
    $c = new CDbCriteria();
    $c->condition = 'post_id=:id or post_parent=:id';
    $c->params = array(':id' => $id);
    $c->order = 'post_id asc';
    
    $count=Post::model()->count($c);
    $pages=new CPagination($count);
    
    /* 
     * Posts per page
     */
    $pages->pageSize=10;
    $pages->applyLimit($c);
    $posts=Post::model()->with('user', 'profile')->findAll($c);
    
    /* 
     * Get forum information
     */
    $f=Forum::model()->findAll(
			       array(
				     'condition' => 'forum_id = :id', 
				     'params' => array(':id'=>$forum)
				     )
			       );
    foreach($f as $row){ $forum_name = $row->forum_name; }
    
    /* 
     * Set breadcrumb navigation, can be very useful
     */
    $this->breadcrumbs=array(
			     'Board index'=>array('forum/index'),
			     $forum_name => array('forum/'.$forum),
			     $title
			     );
    /* 
     * Render that nigga!
     */
    $this->render('topic_view', array(
				      'posts' => $posts,
				      'pages' => $pages,
				      'title' => $title,
				      'model' => $model /* used for forms */
				      )
		  );
  }
  
  /*
   * Action called when a user wants to see a thread
   * Uses dpage to display the thread, this just deals with the requests
   * Also called to post a reply to a thread, if the user is logged in.
   */
  public function actionThread(){
    $id=$_GET['id']; /* /thread?id=33 */
    $model=new Post;
    $post=Post::model()->findAll(
				 array(
				       'condition' => 'post_id=:id AND post_isthread=1', 
				       'params' => array(':id'=>$id)
				       )
				 );
    $x=0;
    
    /* if the post exists in the Db */
    
    if($post){
      
      foreach($post as $a){
	$title = $a->post_title;
	$forum = $a->post_forumid;
      }
      if($_POST){
	if(!Yii::app()->user->isGuest){
	  $model->scenario = 'reply';
	  /*
	   * Validate input, with the 'reply' scenario
	   * shorter post text max len
	   * title not required
	   */
	    $model->post_text = $_POST['Post']['post_text'];
	    $model->post_parent = $id;
	    $model->post_isthread = 0;
	    $model->post_authorid = Yii::app()->user->id;
	    $model->post_forumid = $forum;
	  if($model->validate()){
	    $model->save();
	    /* Redirect to the thread, the new post inside its tight little table
	     */
	    $this->redirect('/testdrive/forum/thread/?page=999&id='.$id);
	  } else {
	    /*
	     * Didn't validate! (oops)
	     */
	    $this->dpage($id, $model, $title, $forum);
	    $x=1;
	  }
	} else {
	  Yii::app()->user->setFlash('notloggedin', "You cannot post replies. Please log in to do so.");
	  $this->redirect('/testdrive/forum/thread/?id='.$id);
	}
      }
      
      
      if(!$x)
	$this->dpage($id, $model, $title, $forum);
      
      /* 
       * Oh no! We couldn't find the thread with the Id
       * Redirect to the forum index with the message.
       */
    } else {
      Yii::app()->user->setFlash('cantfind', "Thread id $id could not be found.");
      $this->redirect('/testdrive/forum');	
    }
  }
  /*
   * Action to post a new thread to a forum with $_GET['id']
   * validate using the special $model->scenario.
   * Check if the user's logged in
   */
  public function actionNew(){
    $y=0;
    $id = (!isset($_GET['id'])) ? '1' : $_GET['id'];

    if(!Yii::app()->user->isGuest){
      $a=Post::model()->findAll(
				array(
				      'condition' => 'post_authorid=:id', 
				      'params' => array(':id'=>Yii::app()->user->id),
				      'order' => 'post_id desc',
				      'limit' => '1'
				      )
				);
      foreach($a as $r) $last_post_time = strtotime($r->post_time);
      if((time()-$last_post_time) < 20){
	$this->redirect('/testdrive/forum/view?id='.$id);
	$y=1;
      }
      if(!$y){
	$model=new Post;
	$forum=Forum::model()->findAll(
				       array(
					     'condition' => 'forum_id=:id', 
					     'params' => array(':id'=>$id)
					     )
				       );
	
	if($forum){
	  foreach($forum as $row) $forum_name = $row->forum_name;
	  $x=0; // flag for shit when its not a fresh page load.
	  /* set variables such as forum id etc. */
	  if($_POST){
	    /* if there is a post request 
	     * declare $model props then save();
	     * set the scenario for required title..
	     */
	    $x = 1;
	    $model->scenario = 'topic';
	    $model->post_title = $_POST['Post']['post_title'];
	    $model->post_text = $_POST['Post']['post_text'];
	    $model->post_authorid = Yii::app()->user->id;
	    $model->post_forumid = $id;
	    $model->post_parent = 0;
	    $model->post_isthread = 1;
	    
	    if($model->validate()){
	      $x=1;
	      $model->save();
	      /*
	       * Get the Id of the user's post by filtering it by 
	       * - their userId
	       * - limit 1
	       * - ordering it by post_id desc (largest post_id at the top)
	       */
	      $f=Post::model()->findAll(
					array(
					      'condition' => 'post_authorid=:id', 
					      'params' => array(':id'=>Yii::app()->user->id),
					      'order' => 'post_id desc',
					      'limit' => '1'
					      )
					);
	      foreach($f as $row) $new_id = $row->post_id;
	      /*
	       * Go in to the view with the new post id
	       */
	      $this->render('goto_new', array('new_id' => $new_id));
	    } else {
	      $x=1;
	      $this->render('new_topic', array(
					       'model' => $model,
					       'id' => $id, 
					       'forum' => $forum_name,
					       ));
	    /* showe new post view again, using variables set before */
	    }
	  }
	  
	  if(!$x){
	    $this->render('new_topic', array(
					     'model' => $model,
					     'id' => $id, 
					     'forum' => $forum_name,
					     ));
	  }
	} else {
	  Yii::app()->user->setFlash('cantfind', "Forum id $id could not be found.");
	  $this->redirect('/testdrive/forum');
	}
      } else {
	/* 
	 * In case the user is somehow able to ignore redirects
	 */
	Yii::app()->user->setFlash('cantfind', "You must be wait before posting again...");
	$this->redirect('/testdrive/forum');      
    }
    } else {
      Yii::app()->user->setFlash('cantfind', "You must be logged in to post.");
      $this->redirect('/testdrive/forum');      
    }
  }
}
?>