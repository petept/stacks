<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title><?php echo CHtml::encode($this->pageTitle); ?></title>
      <meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	  <script>
	  function show_fulldate(newdate, id) {
	  nid = '#'+id;
	  $(nid).fadeOut(function() {
	  $(this).text(newdate).fadeIn('fast');
	  });
	  }
	  $('a').click(function showLogin(){
	  $('#slidein_login').toggle('slide',300);
	  });   
	</script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/Markdown.Converter.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/Markdown.Sanitizer.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/Markdown.Editor.js"></script>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl;?>/site/Usrstyle?id=<?=$this->id;?>">
	
	
	
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/md.css">
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/modernizr-2.5.3.min.js"></script>
	<link rel="stylesheet/less" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/test.less">
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/less-1.3.0.min.js"></script>
      </head>
      <body>
	<!--[if lt IE 7]>
	    <p>The browser you are using is nearly <em>12 years old</em>. 
	    That's like 50 years in Internet time. Please upgrade to a better browser! 
	    <a href="http://browsehappy.com">Go to browsehappy.com to learn how!</a></p>
	    <![endif]-->
	<header>
	  <div class="container">
	    <div class="row">
	      <div class="twelvecol" id="title_bar">
		<img src="/testdrive/css/stacker_main.png" alt="Stacker" />
		
		
		<div id="menu-top">
		  <?php $this->widget('zii.widgets.CMenu',array( 
		    'itemCssClass' => 'top_list_item', 'firstItemCssClass' =>
		    'first', 'activeCssClass' => 'active_menu',
		    'linkLabelWrapper' => 'span',
		    'items'=>array(
		    array('label'=>'Home', 'url'=>array('/site/index'),
  		    ),
		    array('label'=>'Forums','url'=>array('/forum')),
		    array('label'=>'Settings', 'url'=>array('/site/settings'), 'visible'=>!Yii::app()->user->isGuest),
		    array('label'=>'Login', 'url'=>array('/site/login'),
		    'visible'=>Yii::app()->user->isGuest, 'linkOptions' =>
		    array('id' => 'login_activate')),
		    array('label'=>'Me', 'url'=>array('/profile/'.Yii::app()->user->id), 'visible'=>!Yii::app()->user->isGuest),
		    array('label'=>'Join', 'url'=>array('/site/newusr'), 'visible'=>Yii::app()->user->isGuest),
		    array('label'=>'Logout('.Yii::app()->user->name.')','url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
		    ),
		    ));
		  ?>
		</div>
	      </div>
	    </div>
	  </div>
	</header>
	<div class="container mpart">
	  <div class="row">
	    <div class="onecol">
	    </div>
	    
	    <div class="tencol last">
	      <div class="row">
		
		<?php echo $content; ?>
		
	      </div>
	    </div>
	    <div class="onecol">
	    </div>
	  </div>
	</div>
	
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/md.css" />
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo Yii::app()->request->baseUrl; ?>js/libs/jquery-1.7.1.min.js"><\/script>')</script>
	
	<!-- scripts concatenated and minified via build script -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>js/plugins.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>js/script.js"></script>
	<!-- end scripts -->
	
	<!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
	     mathiasbynens.be/notes/async-analytics-snippet -->
	<script>
	  var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
	  (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
	  g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	  s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>
	<script type="text/javascript">
	  (function () {
	  var converter1 = Markdown.getSanitizingConverter();
	  var editor1 = new Markdown.Editor(converter1);
	  editor1.run();
	  
	  var converter2 = new Markdown.Converter();
	  
	  converter2.hooks.chain("preConversion", function (text) {
	  return text.replace(/\b(a\w*)/gi, "*$1*");
	  });
	  
	  converter2.hooks.chain("plainLinkText", function (url) {
	  return "This is a link to " + url.replace(/^https?:\/\//, "");
	  });
	  
	  var help = function () { alert("Do you need help?"); }
	  
	  var editor2 = new Markdown.Editor(converter2, "-second", { handler: help });
	  
	  editor2.run();
	  })();
        </script>
	
      </body>
    </html>
    
    