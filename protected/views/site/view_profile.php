<?
$a=1;
$contact_list = array();
foreach($contacts as $contact){
  array_push($contact_list, $contact['id']);
}
array_push($contact_list,$user['id']);
echo "<h1>".$user['username']."</h1>";
?>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="success profile_saved">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?
$control=array(
	       '0' => $a==1,
	       '1' => $a==1,
	       '2' => !Yii::app()->user->isGuest,
	       '3' => !Yii::app()->user->isGuest and in_array(Yii::app()->user->id, $contact_list)
	       );
$setting = $control[$user['privacy']];
if($setting){

  if($user['privacy'] == 3)
    echo "<div class='info'>This user restricts their profile to those in their contacts list.</div>";

?>
<div class="ninecol">
  <div class="box boxpart">
<h2>Contacts</h2>
<?
  if(count($contacts) > 0){
  echo '<ul class="noli contacts">';
  foreach($contacts as $contact){
  echo "<li><a href=\"".$contact['id']."\"><img width='30' height='30' src='".Avatar::serve($contact['email'], '30')."' /></a>";
  if(!Yii::app()->user->isGuest and $user['id']==Yii::app()->user->id){
  //echo " [<span class='italic'><a href='../site/delc?id=".$contact['id']."'>Remove</a></span>]</li>";
  }
  }
  echo '</ul>';
  } else
  echo "<div class='info'>This user has no contacts.</div>";
?>
  </div>
</div>
<div class="threecol last">
  <div class="box aboutuser boxpart">
    
    <p><?  
      $this->beginWidget('CMarkdown', array('purifyOutput'=>true));
  echo $user['aboutme'];
  $this->endWidget();
    ?></p>
  
    <p><?=$user['website'];?></p>
    <p>Member since <?=$user['join'];?></p>
    <p>Rate 
<?/*
	  $u = Time::usrPostCount($user['id']);
	  if($u[0]!=0){
	  echo ((($u[1]/$u[0])+strtotime($user['join']))/10000)/(1/100*(time()*60-($last_login*60)));
	  echo '<br />'.(time()*60-($last_login*60));
	  } else {
	  echo '0';} */

?>
0.045</p>
    <p>My birthday is
<?
	$d_m = $user['birthday'];
	echo date('jS F', strtotime('2012-'.$d_m));
	?>
    </p>
  </div>
</div>
<?
  
  } 
  elseif($user['privacy'] == 2) {
  echo "<div class='error'>This profile is only viewable to logged in persons.</div>";
  }
  elseif($user['privacy'] == 3) {
  echo "<div class='error'>This profile is restricted to those in this user's contact list.</div>";
  }
  $vc = array();
  foreach($viewer_contacts as $row){
  array_push($vc, $row['id']);
  }
  if((!Yii::app()->user->isGuest) and (!in_array($user['id'], $vc)) and $user['id'] != Yii::app()->user->id){
  echo "<a href=\"/testdrive/site/addc?c=".$user['id']."\">Add ".$user['username']." to contact list.</a>";
  }
  if(in_array($user['id'], $vc))
  echo "<div class='success'>This user is in your conntacts.</div>";
?>
