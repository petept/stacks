<h1>Tag search</h1>
<ul class="tags">
  <li>Search tags by name:
  <ul>
    <li>eg. tag1 tag2 tag3</li>
    <li>must be separated by spaces.</li>
    <li>tags which are not separated by spaces are interpreted as one big tag</li>
  </ul>
  </li>
  <li>Use * in a search for a wildcard:
  <ul>
    <li>eg. another_*</li>
    <li>* will match any number of characters</li>
    <li>can only be used at the beginning or end of tags
    <ul>
      <li>Right: *nother_*</li>
      <li>Wrong: an*er</li>
    </ul>
    </li>
    <li>SQL: <code>WHERE (`t0`.`tag` LIKE 'tag1%')</code></li>
  </ul>
  </li>
  <li>Search by ID:
  <ul>
    <li>id:&gt;4 shows posts with an ID greater than 4 </li> 
    <li>id:&gt;=4 shows posts with an ID greater than or equal to 4</li>
    <li>id:4 <em>or</em> id:=4 shows posts with an ID equal to 4</li>
    <li>id:&lt;4 shows posts with an ID less than 4</li>
    <li>id:&lt;=4 shows posts with an ID less than or equal to 4</li>
    <li>If <code>id:[o][n]</code> where [o] is the operand (eg >) and [n] is the number (eg 4), SQL: <code>WHERE (image_id[o][n])</code></li>
  </ul>
  </li>
  <li>Search between IDs:
  <ul>
    <li>id:5,16 shows posts with an ID between 5 and 16, including 5 and 16</li>
    <li>id:2, shows posts with an ID between 2 and 99999999</li>
    <li>id:2 shows posts with an ID between 2 and 99999999</li>
    <li>if the number after the comma is smaller than the one before it, no results will be returned</li>
    <li>If <code>idb:[l],[h]</code> where [l] is the lower bound and [h] is the upper bound, SQL: <code>WHERE (image_id BETWEEN [l] AND [h])</code>. 
    If [h] is missing, then SQL:  <code>WHERE (image_id BETWEEN [l] AND 99999999)</code></li>
  </ul>
  </li>
  <li>Search posts with user IDs: (same syntax as searching by Id)
  <ul>
    <li>eg. userid:&gt;=4 shows posts with a user Id greater than or equal to 4.</li>
  </ul>
  </li>
</ul>
<h2>Experimental</h2>
<ul class="tags">
<li>Exclude tags using <code>not:tag</code>
<ul>
  <li>eg. not:tag3 shows posts not containing the tag 'tag3'</li>
</ul>
</li>
</ul>
    