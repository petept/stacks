<h2>Edit account settings</h2>
<?php echo CHtml::errorSummary($model); ?>
<p>
  Avatars (user images) are provided with Gravatar, which I strongly recommend that you use; go <a href="https://en.gravatar.com/site/signup/">here</a> to get one. Use the email you provided with your account.
</p>
<div class="registration_form">
  <form method="post" action="/testdrive/index.php/site/settings">
    <table>
      <tr>
	<td>About me</td>
	<td><textarea  name="Settings[aboutme]"><?=$user['aboutme'];?></textarea></td>
      </tr>
      <tr>
	<td>Website address</td>
	<td><input value="<?=$user['website'];?>" type="text" name="Settings[website]" /></td>
      </tr>
      <tr>
	<td>Privacy</td>
	<td>
	  <input type="radio" name="Settings[privacy]" value="1" <? if(($user['privacy'] == 1) or ($user['privacy'] == '')) echo "checked"; ?> />Everyone can view my profile<br>
	  <input type="radio" name="Settings[privacy]" value="2" <? if($user['privacy'] == 2) echo "checked"; ?> /><em>Only</em> registered users can view my profile<br />
	  <input type="radio" name="Settings[privacy]" value="3" <? if($user['privacy'] == 3) echo "checked"; ?> />Only contacts
	</td>
      </tr>
      <tr>
	<td>Profile CSS</td>
	<td><textarea class="code" name="Settings[css]"><?=$user['css'];?></textarea></td>
      </tr>
      <tr>
	<td><input class="button" type="submit" value="Update"/></td>
      </tr>
    </table>
  </form>
</div>
<h2>Login history</h2>
<table>
  <tr>
    <td>IP addr</td>
    <td>Time</td>
  </tr>
<?
   foreach($logins as $login){
   echo '<tr>';
   echo '<td>'.$login->ip.'</td>';
   echo '<td>'.$login->time.'</td>';
   echo '</tr>';
   } 
?>
</table>