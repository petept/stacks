<?php echo CHtml::errorSummary($model); ?>
<h2>Register a new nick</h2>
<div class="registration_form">
  <p class="italic">Fields marked with <span class="required">*</span> are required fields.</p>
  <form method="post" action="/testdrive/index.php/site/newusr">
    <table>
      <tr>
	<td>Näme:</td>
	<td><input type="text" name="Registration[username]" /></td>
      </tr>
      <tr>
	<td>Birthday (dd-mm)</td>
	<td><input type="text" name="Registration[dob]" /></td>
      <tr>
	<td>Email address</td>
	<td><input type="email" name="Registration[email]" /><span class="required">*</span></td>
      </tr>
      <tr>
	<td>Password</td>
	<td><input type="password" name="Registration[password]" /><span class="required">*</span></td>
      </tr>
      <tr>
	<td>Confirm password</td>
	<td><input type="password" name="Registration[password_confirm]" /><span class="required">*</span></td>
      </tr> 
      <tr>
	<td><input type="submit" class="button" value="Create" /></td>
      </tr>
    </table>
  </form>
</div>