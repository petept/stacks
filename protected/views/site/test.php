<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'forum-test-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'forum_name'); ?>
		<?php echo $form->textField($model,'forum_name'); ?>
		<?php echo $form->error($model,'forum_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'forum_description'); ?>
		<?php echo $form->textField($model,'forum_description'); ?>
		<?php echo $form->error($model,'forum_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'forum_slug'); ?>
		<?php echo $form->textField($model,'forum_slug'); ?>
		<?php echo $form->error($model,'forum_slug'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'forum_posts'); ?>
		<?php echo $form->textField($model,'forum_posts'); ?>
		<?php echo $form->error($model,'forum_posts'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'forum_parent'); ?>
		<?php echo $form->textField($model,'forum_parent'); ?>
		<?php echo $form->error($model,'forum_parent'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->