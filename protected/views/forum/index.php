<?php if(Yii::app()->user->hasFlash('cantfind')):?>
    <div class="error">
        <?php echo Yii::app()->user->getFlash('cantfind'); ?>
    </div>
<?php endif; ?>
<table class="forum">
<tr>
<th>Forum</th>
<th width="5%">Posts</th>
</tr>  
<?
       $i=0;
    foreach($forums as $forum){
       if($i % 2) $class = "even";
       else $class="odd";
   echo '<tr class="forum_list_row '.$class.'"><td>';
   echo '<a class="forum_link" href="/testdrive/forum/view?id='.$forum->forum_id.'">'.$forum->forum_name,'</a>';
   echo '<p>'.$forum->forum_description.'</p>';
   echo '</td><td>';
   echo $forum->forum_posts;
   echo '</td></tr>';
   $i++;
 }
  ?>
</table>