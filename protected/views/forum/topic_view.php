<?
/* 
 * Thread title
 */
echo '<h1>'.$title.'</h1>';
$i=0;
$post_counts=array();
$reputations=array();

foreach($posts as $post){
  $userid=$post->user->id;
  $t=Time::usrPostCount($userid);
  $post_counts[$userid] = $t;

}

foreach($posts as $post){
  $i++;
  if($i==1)
    echo '<div class="post first">';
  elseif($i==count($posts))
    echo '<div class="post last" style="clear:both;">';
  else
    echo '<div class="post" style="clear:both;">';
?>
<div>
<div class="row">
<div class="ninecol">
<?   echo ' <span onmouseover="show_fulldate(\''.date("H:m jS F Y \G\M\T", strtotime($post->post_time)).'\', '.$post->post_id.')" onmouseout="show_fulldate(\''.Time::timeago(strtotime($post->post_time)).'\', '.$post->post_id.')" 
id="'.$post->post_id.'" 
class="forum_post_time" style="float:right;">'.Time::timeago(strtotime($post->post_time)).'</span>'; ?>
<?
    // echo $post->user->role;
 $this->beginWidget('CMarkdown', array('purifyOutput'=>true));
  echo $post->post_text;
  $this->endWidget();
?>
</div>
<div class="threecol last forum_user_info" style="margin:0;">
<?
echo CHtml::link('<div class="post_avatar" style="background-image:url('.Avatar::serve($post->user->email, '35').'); float:left;margin-right:5px;"></div>', array('/site/profile', 'id'=>$post->user->id), array('class'=>'user_link'));
?>
    <ul class="no_list">
       <li><?
       echo CHtml::link($post->user->username, array('/site/profile', 'id'=>$post->user->id), array('style' => 'text-decoration:none;'));
?></li>
    <li>Joined: <? echo date('jS F Y', strtotime($post->user->join)); ?></li>
    <li>Posts: <?=$post_counts[$post->user->id][0].':'.$post_counts[$post->user->id][1].':'.$post_counts[$post->user->id][2];?></li>
</ul>
 
</div>
</div>
</div>
<?

  echo '</div>'; /* /post */
}
?>
<?
/*
 * If the user isn't a guest, display the form
 */
if(!Yii::app()->user->isGuest){
?>
<h4>Reply</h4>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
 
    <?php echo $form->errorSummary($model); ?>
 
 
    <div class="row">
    <div class="wmd-panel">
            <div id="wmd-button-bar"></div>
    <?php echo $form->textArea($model,'post_text', array('class' => 'wmd-input', 'id'=>'wmd-input')) ?>
    </div>
    <div id="wmd-preview" class="wmd-panel wmd-preview"></div>
    </div>
 
 
    <div class="row submit">
        <?php echo CHtml::submitButton('Post reply'); ?>
    </div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->
    <? } 
 /* 
  * Show pagination links
  */
$this->widget('CLinkPager', array(
    'pages' => $pages,
  ));
?>
