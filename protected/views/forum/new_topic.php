<h2>Post new topic: <?=$forum;?></h2>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
 
    <?php echo $form->errorSummary($model); ?>
 
    <div class="row">
   <label for="Post[post_title]">Title</label>
   <?php echo $form->textField($model,'post_title', array('placeholder' => 'Title')) ?>
    </div> 
    <div class="row">
   
   <label for="Post[post_text]">Message</label>
                  <div class="wmd-panel">
            <div id="wmd-button-bar"></div>
   <?php echo $form->textArea($model,'post_text', array('class' => 'wmd-input', 'id'=>'wmd-input')) ?>
        </div>
        <div id="wmd-preview" class="wmd-panel wmd-preview"></div>
    </div>
 
 
    <div class="row submit">
        <?php echo CHtml::submitButton('Post new'); ?>
    </div>
 
<?php $this->endWidget(); ?>
</div><!-- form -->

