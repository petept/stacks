<?php

/**
 * This is the model class for table "tbl_posts".
 *
 * The followings are the available columns in table 'tbl_posts':
 * @property string $post_title
 * @property integer $post_isthread
 * @property integer $post_id
 * @property string $post_text
 * @property integer $post_authorid
 */
class TblPosts extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TblPosts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_posts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('post_title, post_isthread, post_text, post_authorid', 'required'),
			array('post_isthread, post_authorid', 'numerical', 'integerOnly'=>true),
			array('post_title', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('post_title, post_isthread, post_id, post_text, post_authorid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'post_title' => 'Post Title',
			'post_isthread' => 'Post Isthread',
			'post_id' => 'Post',
			'post_text' => 'Post Text',
			'post_authorid' => 'Post Authorid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('post_title',$this->post_title,true);
		$criteria->compare('post_isthread',$this->post_isthread);
		$criteria->compare('post_id',$this->post_id);
		$criteria->compare('post_text',$this->post_text,true);
		$criteria->compare('post_authorid',$this->post_authorid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}