<?php

/**
 * This is the model class for table "tbl_profile".
 *
 * The followings are the available columns in table 'tbl_profile':
 * @property integer $profile_id
 * @property string $aboutme
 * @property string $website
 */
class Profile extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Profile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			     array('privacy', 'required'),
			     //array('aboutme, website', 'required'),
			array('aboutme', 'length', 'max'=>250),
			array('aboutme', 'length', 'min'=>10),
			array('aboutme', 'required'),
			array('website', 'length', 'max'=>500),
			array('website', 'url'),
			array('privacy', 'numerical', 'integerOnly'=>1),
			array('privacy', 'numerical', 'min'=>1),
			array('privacy', 'numerical', 'max'=>3),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('profile_id, aboutme, website', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			     'contacts'=>array(self::HAS_MANY, 'Contacts', 'owner'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'profile_id' => 'Profile',
			'aboutme' => '\'About me\'',
			'website' => 'Website',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('profile_id',$this->profile_id);
		$criteria->compare('aboutme',$this->aboutme,true);
		$criteria->compare('website',$this->website,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}