<?php

/**
 * This is the model class for table "tbl_mess".
 *
 * The followings are the available columns in table 'tbl_mess':
 * @property integer $userid
 * @property string $message
 * @property integer $votes
 * @property integer $messid
 */
class Regist extends CActiveRecord
{
  public $password_confirm;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TblMess the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}


	public function beforeSave(){
	  $this->password = md5($this->password);
	  if(trim($this->username) == '')
	    $this->username = $this->email;
	  return true;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email', 'required'),
			array('email', 'email'),
			array('email', 'length', 'max' => '50'),
			array('email', 'unique'),

			array('username', 'length', 'max' => '128'),
			array('username', 'length', 'min' => '3'),
			array('username', 'unique'),

			array('password', 'required'),
			array('password', 'length', 'min' => '5'),
			array('password', 'length', 'max' => '128'),
			
			array('password_confirm', 'compare', 'compareAttribute' => 'password'),
			array('dob', 'date', 'format' => 'dd-mm', 'message' => 'The birthday you have entered is invalid. Should be formatted as mm-dd.')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
	  return array(
		       
		       );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'email' => 'Email address',
			'password' => 'Password',
			'password_comfirm' => 'Password confirmation',
			'dob' => 'birthday'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
	  	// Warning: Please modify the following code to remove attributes that
		// should not be searched.

	  /*	$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('votes',$this->votes);
		$criteria->compare('messid',$this->messid);
	  */
		return new CActiveDataProvider($this, array(
			
		));
	}
}