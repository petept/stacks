<?php

/**
 * This is the model class for table "tbl_mess".
 *
 * The followings are the available columns in table 'tbl_mess':
 * @property integer $userid
 * @property string $message
 * @property integer $votes
 * @property integer $messid
 */
class TblMess extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TblMess the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_mess';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message', 'required'),
			array('votes, messid', 'numerical', 'integerOnly'=>true),
			array('message', 'length', 'max'=>5),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, message, votes, messid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
	  return array(
		       'author'=>array(self::BELONGS_TO, 'User', 'userid'),
		       );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'message' => 'Message',
			'votes' => 'Votes',
			'messid' => 'Messid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('votes',$this->votes);
		$criteria->compare('messid',$this->messid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}