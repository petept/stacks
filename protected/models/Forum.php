<?php

/**
 * This is the model class for table "tbl_forums".
 *
 * The followings are the available columns in table 'tbl_forums':
 * @property integer $forum_id
 * @property string $forum_name
 * @property string $forum_description
 * @property string $forum_slug
 * @property integer $forum_posts
 * @property integer $forum_parent
 */
class Forum extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Forum the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_forums';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('forum_name, forum_description, forum_slug, forum_posts, forum_parent', 'required'),
			array('forum_posts, forum_parent', 'numerical', 'integerOnly'=>true),
			array('forum_name', 'length', 'max'=>100),
			array('forum_description', 'length', 'max'=>250),
			array('forum_slug', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('forum_id, forum_name, forum_description, forum_slug, forum_posts, forum_parent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'forum_id' => 'Forum',
			'forum_name' => 'Forum Name',
			'forum_description' => 'Forum Description',
			'forum_slug' => 'Forum Slug',
			'forum_posts' => 'Forum Posts',
			'forum_parent' => 'Forum Parent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('forum_id',$this->forum_id);
		$criteria->compare('forum_name',$this->forum_name,true);
		$criteria->compare('forum_description',$this->forum_description,true);
		$criteria->compare('forum_slug',$this->forum_slug,true);
		$criteria->compare('forum_posts',$this->forum_posts);
		$criteria->compare('forum_parent',$this->forum_parent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}