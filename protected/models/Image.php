<?php

/**
 * This is the model class for table "images".
 *
 * The followings are the available columns in table 'images':
 * @property integer $image_id
 * @property string $image_name
 * @property integer $image_author
 * @property string $image_path
 */
class Image extends CActiveRecord
{
  public $image;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Image the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'images';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			     array('image', 'file', 'types'=>'jpg, gif, png'),
			     //array('image', 'allowEmpty', 'false'),
			     array('image_cached_tags, image_author, image_title', 'required'),
			     array('image_author', 'numerical', 'integerOnly'=>true),
			     array('image_title', 'length', 'max'=>50),
			     array('image_path', 'length', 'max'=>255),
			     array('image_cached_tags', 'checkTags'),
			     // The following rule is used by search().
			     // Please remove those attributes that should not be searched.
			     array('image_id, image_name, image_author, image_path', 'safe', 'on'=>'search'),
			     );
	}
	public function checkTags($attribute, $params){
	  $tags = explode(" ", $this->image_cached_tags);
	  if(count($tags) < 2)
	    $this->addError('tags','You must specify more than two tags with your image.');
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'image_id' => 'Image',
			'image_title' => 'Image Name',
			'image_author' => 'Image Author',
			'image_path' => 'Image Path',
			'image_cached_tags' => 'Tags'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('image_id',$this->image_id);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('image_author',$this->image_author);
		$criteria->compare('image_path',$this->image_path,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}